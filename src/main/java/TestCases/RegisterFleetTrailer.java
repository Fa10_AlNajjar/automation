package TestCases;
import Initiate.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.Test;
import java.awt.*;
public class RegisterFleetTrailer extends BaseTest {
     @Test (priority = 1,description = "Happy Scenario",testName = "Happy Register Trailer")
    public void HappyRegister() throws InterruptedException, AWTException {
        Thread.sleep(500);
        ReachTrailerFromMenu.OPOFF();
         Owner_Info.OI();
         //to perform Scroll on application using  Selenium
         JavascriptExecutor js = (JavascriptExecutor) driver;
         js.executeScript("window.scrollBy(0,-250)", "");
         InsuranceTab.Insurance_Info();
         attachment.FrontsideIamge();
         Thread.sleep(2500);
         attachment.BackSideImage();
         Thread.sleep(2500);
         attachment.VEC_Photo_No1();
         Thread.sleep(2500);
         attachment.VEC_Photo_No2();
         Thread.sleep(1500);
         attachment.VEC_Photo_No3();
         Thread.sleep(1500);
         attachment.VEC_Photo_No4();
         Thread.sleep(1500);
         attachment.InspectionDocument();
         Thread.sleep(1000);
         attachment.AUTHORIZATION();
         Thread.sleep(1000);
         //to perform Scroll on application using  Selenium
         js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
         driver.findElement(By.id("CREATE")).click();
         Thread.sleep(1500);
         driver.findElement(By.xpath("/html/body/div[10]/div/div[3]/button[1]")).click();
         Thread.sleep(1500);
        }
}
