package TestCases;

import Initiate.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.Random;

public class Owner_Info extends BaseTest {
    static Random rand = new Random();
    static int rnd = rand.nextInt(999) + 1;
    public static void OI() throws InterruptedException {
        Thread.sleep(500);
        driver.findElement(By.name("LICENSE_EXPIRY_FROM")).sendKeys("01/09/2020");
        driver.findElement(By.name("LICENSE_EXPIRY_TO")).sendKeys("30/09/2020");
        driver.findElement(By.id("PLATE_NUMBER")).sendKeys(rnd + "1");
        //Owner nae=me
        Select Selector0 = new Select(driver.findElement(By.id("RELATED_ENTITY_RECORD_ID")));
        Selector0.selectByValue("61");
        WebElement selected0 = Selector0.getFirstSelectedOption();
        System.out.println("Selected Value is" + selected0.getText());
        //Select Plate Code
        Thread.sleep(1000);
        Select Selector = new Select(driver.findElement(By.id("FLEET_PLATE_CODE")));
        Selector.selectByIndex(2);
        WebElement selected = Selector.getFirstSelectedOption();
        System.out.println("Plate Code is >>  " + selected.getText());
        //Select Usage
        Select Selector1 = new Select(driver.findElement(By.id("CATEGORY")));
        Selector1.selectByIndex(2);
        WebElement selected1 = Selector1.getFirstSelectedOption();
        System.out.println("Usage is " + selected1.getText());
        //Select PLATE_TYPE_CODE
        Select Selector2 = new Select(driver.findElement(By.id("PLATE_TYPE_CODE")));
        Selector2.selectByIndex(2);
        WebElement selected2 = Selector2.getFirstSelectedOption();
        System.out.println("Plate Type is << " + selected2.getText());
        //select model year MODEL_YEAR
        Thread.sleep(500);
        Select Selector3 = new Select(driver.findElement(By.id("MODEL_YEAR")));
        Selector3.selectByIndex(2);
        WebElement selected3 = Selector3.getFirstSelectedOption();
        System.out.println("Model year is" + selected3.getText());
        //Select COLOR_CODE
        Thread.sleep(500);
        Select Selector4 = new Select(driver.findElement(By.id("COLOR_CODE")));
        Selector4.selectByIndex(2);
        WebElement selected4 = Selector4.getFirstSelectedOption();
        System.out.println("Fleet Color is" + selected4.getText());
        //Select SUBCOLOR
        Select Selector5 = new Select(driver.findElement(By.id("SUBCOLOR")));
        Selector5.selectByIndex(2);
        WebElement selected5 = Selector5.getFirstSelectedOption();
        System.out.println("Sub color is" + selected5.getText());
        driver.findElement(By.id("TRAILER_LENGTH")).sendKeys("19");
        driver.findElement(By.id("EXTREME_CAPACITY")).sendKeys("5555");
        //Select MANUFACTURING_YEAR
        Select Selector7 = new Select(driver.findElement(By.id("MANUFACTURING_YEAR")));
        Selector7.selectByIndex(2);
        WebElement selected7 = Selector7.getFirstSelectedOption();
        System.out.println("MANUFACTURING_YEAR is " + selected7.getText());
        //ORIGIN_COUNTRY_CODE
        Select Selector8 = new Select(driver.findElement(By.id("ORIGIN_COUNTRY_CODE")));
        Selector8.selectByIndex(2);
        WebElement selected8 = Selector8.getFirstSelectedOption();
        System.out.println("ORIGIN_COUNTRY_CODE Is " + selected8.getText());
        driver.findElement(By.id("AXES_NUMBER")).sendKeys("1");
        driver.findElement(By.id("CHASSES_NUMBER")).sendKeys(rnd + "215");
        driver.findElement(By.id("EMPTY_WEIGHT")).sendKeys("1");
    }
}