package TestCases;
import Initiate.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;
public class InsuranceTab extends BaseTest {
    @Test
    public static void Insurance_Info () throws InterruptedException {
        //Insurance info tab
        driver.findElement(By.xpath("//*[@id=\"INSURANCEINFO-TAB\"]")).click();
        Thread.sleep(3000);
        driver.findElement(By.id("INSURANCE_COMPANY")).sendKeys("INSURANCE_COMPANY");
        driver.findElement(By.id("INSURANCE_DOCUMENT_NUMBER")).sendKeys("INSURANCE_DOCUMENT_NUMBER");
        Select Selector = new Select(driver.findElement(By.id("INSURANCE_TYPE")));
        Selector.selectByValue("COMPREHENSIVE_COVERAGE");
        WebElement selected = Selector.getFirstSelectedOption();
        System.out.println("Insurance type is" + selected.getText());
        driver.findElement(By.id("FIRST_REGISTRATION_DATE")).sendKeys("30/08/2020");
    }
}




