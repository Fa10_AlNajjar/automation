package TestCases;

import Initiate.BaseTest;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;

public class attachment extends BaseTest {
    @Test
    public static void FrontsideIamge() throws InterruptedException, AWTException {
       // OpenRegistrationTrailerForm.OPOFF();
        driver.findElement(By.id("ATTACHMENT-TAB")).click();
        //Upload image
        Thread.sleep(1000);
        Robot robot = new Robot();
        driver.findElement(By.id("TRAILER_LICENSE_FRONT")).click();
        robot.setAutoDelay(1000);
        //copy the image to the clipboard
        StringSelection stringselection = new StringSelection("C:\\Users\\user\\IdeaProjects\\omanweb\\src\\main\\resources\\attachment\\FrontSide.jpg");
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringselection, null);
        robot.setAutoDelay(1000);
        //paste the image
        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_V);
        //press enter button
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);
       // robot.setAutoDelay(1000);

    }
    public static void BackSideImage () throws InterruptedException, AWTException {
        //Upload image
        Thread.sleep(1000);
        Robot robot = new Robot();
        driver.findElement(By.id("TRAILER_LICENSE_BACK")).click();
        robot.setAutoDelay(1000);
        //copy the image to the clipboard
        StringSelection stringselection = new StringSelection("C:\\Users\\user\\IdeaProjects\\omanweb\\src\\main\\resources\\attachment\\BackSide.png");
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringselection, null);
        robot.setAutoDelay(1000);
        //paste the image
        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_V);
        //press enter button
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);
       // robot.setAutoDelay(1000);
    }
    //VEHICLE_PHOTO_NO1
    public static  void VEC_Photo_No1 () throws InterruptedException, AWTException {
        Thread.sleep(1000);
        Robot robot = new Robot();
        driver.findElement(By.id("VEHICLE_PHOTO_NO1")).click();
        robot.setAutoDelay(1000);
        //copy the image to the clipboard
        StringSelection stringselection = new StringSelection("C:\\Users\\user\\IdeaProjects\\omanweb\\src\\main\\resources\\attachment\\V1.jpg");
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringselection, null);
        robot.setAutoDelay(1000);
        //paste the image
        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_V);
        //press enter button
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);
    }
    //VEHICLE_PHOTO_NO2
    public static void VEC_Photo_No2 () throws InterruptedException, AWTException {
        Thread.sleep(1000);
        Robot robot = new Robot();
        driver.findElement(By.id("VEHICLE_PHOTO_NO2")).click();
        robot.setAutoDelay(1000);
        //copy the image to the clipboard
        StringSelection stringselection = new StringSelection("C:\\Users\\user\\IdeaProjects\\omanweb\\src\\main\\resources\\attachment\\V2.jpg");
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringselection, null);
        robot.setAutoDelay(1000);
        //paste the image
        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_V);
        //press enter button
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);
    }
    //
    public static void VEC_Photo_No3() throws InterruptedException, AWTException {
        Thread.sleep(1000);
        Robot robot = new Robot();
        driver.findElement(By.id("VEHICLE_PHOTO_NO3")).click();
        robot.setAutoDelay(1000);
        //copy the image to the clipboard
        StringSelection stringselection = new StringSelection("C:\\Users\\user\\IdeaProjects\\omanweb\\src\\main\\resources\\attachment\\V3.jpg");
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringselection, null);
        robot.setAutoDelay(1000);
        //paste the image
        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_V);
        //press enter button
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);
    }
    public static void VEC_Photo_No4 () throws InterruptedException, AWTException {
    //VEHICLE_PHOTO_NO4
    Thread.sleep(1000);
    Robot robot = new Robot();
        driver.findElement(By.id("VEHICLE_PHOTO_NO4")).click();
        robot.setAutoDelay(1000);
    //copy the image to the clipboard
    StringSelection stringselection = new StringSelection("C:\\Users\\user\\IdeaProjects\\omanweb\\src\\main\\resources\\attachment\\V4.jpg");
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringselection, null);
        robot.setAutoDelay(1000);
    //paste the image
        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_V);
    //press enter button
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);
}
public static void InspectionDocument () throws InterruptedException, AWTException {
    Thread.sleep(1000);
    Robot robot = new Robot();
    driver.findElement(By.id("INSPECTION_DOCUMENT")).click();
    robot.setAutoDelay(1000);
    //copy the image to the clipboard
    StringSelection stringselection = new StringSelection("C:\\Users\\user\\IdeaProjects\\omanweb\\src\\main\\resources\\attachment\\Inspection_document.xlsx");
    Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringselection, null);
    robot.setAutoDelay(1000);
    //paste the image
    robot.keyPress(KeyEvent.VK_CONTROL);
    robot.keyPress(KeyEvent.VK_V);
    //press enter button
    robot.keyPress(KeyEvent.VK_ENTER);
    robot.keyRelease(KeyEvent.VK_ENTER);
}
//AUTHORIZATION
    public static void AUTHORIZATION() throws InterruptedException, AWTException {
        Thread.sleep(1000);
        Robot robot = new Robot();
        driver.findElement(By.id("AUTHORIZATION")).click();
        robot.setAutoDelay(1000);
        //copy the image to the clipboard
        StringSelection stringselection = new StringSelection("C:\\Users\\user\\IdeaProjects\\omanweb\\src\\main\\resources\\attachment\\Inspection_document.xlsx");
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringselection, null);
        robot.setAutoDelay(1000);
        //paste the image
        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_V);
        //press enter button
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);
    }
}