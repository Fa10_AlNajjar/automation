package Initiate;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import java.io.FileNotFoundException;
import java.util.Properties;


public class BaseTest {
    protected Properties cloudProperties = new Properties();
    public static WebDriver driver;

    /*@BeforeTest

     public void beforeTestFF() {
        System.setProperty("webdriver.gecko.driver","src/main/resources/geckodriver.exe");
        driver = new FirefoxDriver() ;
    }*/
   @BeforeTest
    public void beforeTest() throws FileNotFoundException {
        System.setProperty("webdriver.chrome.driver","src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.navigate().to(PropertiesReader.ReadData("URL"));

    }

    @AfterTest
    public void afterTest()  {
        driver.quit();
    }

    public static  void LoginAsOPOFF () {
        driver.findElement(By.id("USERNAME")).sendKeys("opoff");
        driver.findElement(By.id("PASSWORD")).sendKeys("123");
        driver.findElement(By.id("Login_Btn")).click();
    }
    public static  void LoginAsOwner_Driver () {
        driver.findElement(By.id("USERNAME")).sendKeys("55132");
        driver.findElement(By.id("PASSWORD")).sendKeys("faten55132");
        driver.findElement(By.id("Login_Btn")).click();
    }
}
